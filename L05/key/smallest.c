#include <stdio.h>
#include <float.h>

int main () {
    double number;
    double smallest = DBL_MAX;
    int count = 0;
    while (scanf ("%lf",&number) == 1) {
        if (number < smallest) {
            smallest = number;
        }
        count += 1;
    }
    if (count > 0) {
        printf ("The smallest number is %lf\n",smallest);
    }
}

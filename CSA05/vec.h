#ifndef VEC_H
#define VEC_H

/* calculates ||u-v||^2 */
double vec_dist_sq (double* u, double* v, int dim);

/* w = u + v */
void vec_add (double* u, double* v, double* w, int dim);

/* w = cv */
void vec_scalar_mult (double* v, double c, double* w, int dim);

/* performs the deep copy v->data[i] = w->data[i] for all i */
void vec_copy (double* v, double* w, int dim);

/* zeros the vector v */
void vec_zero (double* v, int dim);

#endif 


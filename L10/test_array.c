#include <stdio.h>
#include "array.h"

#define SIZE 10

int main () {
    array_type array;
    array_zeros(&array,SIZE);
    for (int i=0;i<SIZE;i++) {
        array_set(&array,i,i+1);
    }
    for (int i=SIZE-1;i>=0;i--) {
        printf ("%.0lf ",array_get(&array,i));
    }
    printf ("\n");
    array_free(&array);
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vec.h"

typedef struct extreme_pair_s {
    double dist_sq;
    int i,j;
} extreme_pair_type;

extreme_pair_type find_extreme_pair (double* data, int rows, int cols) {
    extreme_pair_type extreme_pair = {0,-1,-1};
    for (int i=0;i<rows-1;i++) {
        for (int j=i+1;j<rows;j++) {
            float dist_sq = vec_dist_sq(data+i*cols,data+j*cols,cols);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i = i;
                extreme_pair.j = j;
            }
        }
    }
    return extreme_pair;
}

int main () {

    /* read the shape of the data matrix */
    int rows, cols;
    if (scanf("%*c %d %d",&rows, &cols) != 2) {
        printf ("error reading the shape of the data matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the data matrix */
    /* note: this line is roughly equivalent to */
    /* double data[rows*cols] */
    /* but the data memory is located on the heap rather than the stack */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* read the data matrix */
    for (int i=0;i<rows;i++) {
        for (int j=0;j<cols;j++) {
            if (scanf("%lf",data+i*cols+j) != 1) {
                printf ("error reading data matrix\n");
                return 1;
            }
        }
    }

    /* find the extreme pair */
    extreme_pair_type extreme_pair = find_extreme_pair(data,rows,cols);

    /* output the results */
    printf ("Extreme Distance = %.2f\n",sqrt(extreme_pair.dist_sq));
    printf ("Extreme Pair = %d %d\n",extreme_pair.i,extreme_pair.j);

    /* free the dynamically allocated memory */
    free (data);
}

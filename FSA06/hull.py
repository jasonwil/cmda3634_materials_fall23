import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 3):
    print ("Command Usage : python3",sys.argv[0],"datafile","imagefile")
    exit(1)
datafile = sys.argv[1]
imagefile = sys.argv[2]

# read the points from the data file
pts = np.loadtxt(datafile)

# plot the points 
plt.gca().set_aspect('equal')
plt.scatter(pts[:,0],pts[:,1],s=10,color='black')

# Give a complete implementation of this function
def test_edge(pts, i, j):
    return False

# Loop over all edges and print/plot only those edges on the convex hull
n = len(pts)
for i in range(0,n-1):
    for j in range(i+1,n):
        if test_edge(pts,i,j):
            p1 = pts[i]
            p2 = pts[j]
            print (i,j)
            plt.plot([p1[0],p2[0]],[p1[1],p2[1]],color='red')

# save the plot as an image
plt.savefig(imagefile)


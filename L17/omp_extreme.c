#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "vec.h"

typedef struct extreme_pair_s {
    double dist_sq;
    int i,j;
} extreme_pair_type;

extreme_pair_type find_extreme_pair (double* data, int rows, int cols) {
    extreme_pair_type extreme_pair = {0,-1,-1};
#pragma omp parallel default(none) shared(data,rows,cols,extreme_pair)
    {
        int thread_num = omp_get_thread_num();
        int thread_count = omp_get_num_threads();
        extreme_pair_type thread_extreme_pair = {0,-1,-1};
        int num_pairs = 0;
#ifdef DYNAMIC
#pragma omp for schedule(dynamic)
#else
#pragma omp for schedule(static)
#endif
        for (int i=0;i<rows-1;i++) {
            for (int j=i+1;j<rows;j++) {
                float dist_sq = vec_dist_sq(data+i*cols,data+j*cols,cols);
                if (dist_sq > thread_extreme_pair.dist_sq) {
                    thread_extreme_pair.dist_sq = dist_sq;
                    thread_extreme_pair.i = i;
                    thread_extreme_pair.j = j;
                }
                num_pairs += 1;
            }
        }
#ifndef STUDY
        printf ("Thread %d checked %d pairs\n",thread_num,num_pairs);
#endif
#pragma omp critical
        {
            if (thread_extreme_pair.dist_sq > extreme_pair.dist_sq) {
                extreme_pair = thread_extreme_pair;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    /* get num_points and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"num_points","thread_count");
        return 1;
    }
    int num_points = atoi(argv[1]);
    int thread_count = atoi(argv[2]);
    omp_set_num_threads(thread_count);

    /* read the shape of the data matrix */
    int rows, cols;
    if (scanf("%*c %d %d",&rows, &cols) != 2) {
        printf ("error reading the shape of the data matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the data matrix */
    /* note: this line is roughly equivalent to */
    /* double data[rows*cols] */
    /* but the data memory is located on the heap rather than the stack */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* read the data matrix */
    for (int i=0;i<rows;i++) {
        for (int j=0;j<cols;j++) {
            if (scanf("%lf",data+i*cols+j) != 1) {
                printf ("error reading data matrix\n");
                return 1;
            }
        }
    }

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* find the extreme pair of the first num_points points */
    extreme_pair_type extreme_pair = find_extreme_pair(data,num_points,cols);

    /* stop the timer */
    end_time = omp_get_wtime();

    /* output the results */
#ifndef STUDY
    printf ("num_points = %d\n",num_points);
    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf ("Extreme Distance = %.2f\n",sqrt(extreme_pair.dist_sq));
    printf ("Extreme Pair = %d %d\n",extreme_pair.i,extreme_pair.j);
#else
    printf ("(%d,%.4lf),",thread_count,(end_time-start_time));
#endif

    /* free the dynamically allocated memory */
    free (data);
}

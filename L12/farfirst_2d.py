import sys # for command line arguments
import numpy as np # for matrix processing
import ctypes as ct # for calling C from Python
import matplotlib.pyplot as plt # for visualization

# load C functions for CSA03
clib = ct.cdll.LoadLibrary("./farfirst.so")
clib.calc_cost_sq.restype = ct.c_double

# make sure command line arguments are provided 
if (len(sys.argv) < 3):
    print ('command usage :',sys.argv[0],'datafile','k','(imagefile)')
    exit(1)

# read datafile name from the command line
datafile = sys.argv[1]

# read k from the command line
k = int(sys.argv[2])

# read the points
# float64 is equivalent to a C double
points = np.loadtxt(datafile,comments='#',dtype='float64')
points_cptr = points.ctypes.data_as(ct.POINTER(ct.c_double));
rows,cols = points.shape;

# find k centers using the farthest first algorithm
# int32 is equivalent to a C integer
centers = np.zeros(k,dtype='int32')
centers_cptr = centers.ctypes.data_as(ct.POINTER(ct.c_int));
centers[0] = 0
for m in range(1,k):
    centers[m] = clib.calc_arg_max(points_cptr,ct.c_int(rows),ct.c_int(cols),
            centers_cptr,ct.c_int(m))

# compute the approximate minimal cost squared
cost_sq = clib.calc_cost_sq(points_cptr,ct.c_int(rows),ct.c_int(cols),
        centers_cptr,ct.c_int(k))

# print the approximate minimal cost for the k-center problem
cost = np.round(np.sqrt(cost_sq),4)
print ("approximate optimal cost =",cost);

# print an approx optimal solution to the k-center problem
print ("approx optimal centers: ",end="");
print (centers)

# if image filename in command line arguments, 
# set the aspect ratio to equal and plot the results
if (len(sys.argv) > 3):
    plt.gca().set_aspect('equal')
    dist_sq = np.zeros((len(centers),len(points)))
    for i in range(len(centers)):
        dist_sq[i] = np.sum((points-points[centers[i]])*(points-points[centers[i]]),axis=1)
    clusters = np.argmin(dist_sq,axis=0)
    extreme = np.argmax(np.min(dist_sq,axis=0))
    plt.scatter (points[:,0],points[:,1],c=clusters,cmap="tab10",s=10,alpha=0.5)
    plt.scatter (points[centers,0],points[centers,1],c=range(len(centers)),cmap="tab10",s=100)
    plt.scatter (points[centers,0],points[centers,1],s=100,facecolors='none',edgecolors='black')    
    plt.scatter (points[extreme,0],points[extreme,1],s=50,facecolors='none', edgecolors='black')
    plt.title ('Cost = %g' % cost)
    plt.savefig(sys.argv[3])

#include <stdio.h> 
#include <stdlib.h> 
#include <omp.h>

int main(int argc, char **argv) {
    int N = 8;
    int A[8] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    int B[8] = { -1, 1, -1, 1, -1, 1, -1, 1 }; 
    int result = 0;
    int num_threads = 4;
    omp_set_num_threads(num_threads);
#pragma omp parallel
    {
        int thread_num = omp_get_thread_num();
        int thread_result = 0;
#pragma omp for schedule(static)
        for (int i = 0; i < N; i++) {
            printf ("thread %d computing term %d\n",thread_num,i);
            thread_result += A[i]*B[i]; 
        }
#pragma omp critical
        {
            result += thread_result;
        }
    }
    printf ("result = %d\n",result); 
}

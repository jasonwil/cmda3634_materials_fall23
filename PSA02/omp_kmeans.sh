#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:05:00
#SBATCH --cpus-per-task=4
#SBATCH -o omp_kmeans.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
gcc -o omp_kmeans omp_kmeans.c vec.c -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=TRUE

# run omp_kmeans
cat $1 | ./omp_kmeans $2 $3 4

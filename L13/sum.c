#include <stdio.h>
#include <stdlib.h>

typedef unsigned long long uint64_t;

int main(int argc, char **argv) {

    /* get N from command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64_t N = atol(argv[1]);

    /* calculate the sum */
    uint64_t sum = 0;
    for (uint64_t i = 1; i <= N;i++) {
        sum += i;
    }

    printf ("sum = %llu\n",sum);
    printf ("N*(N+1)/2 = %llu\n",(N/2)*(N+1));
}


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "vec.h"

typedef struct extreme_pair_s {
    double dist_sq;
    int i[2];
} extreme_pair_type;

extreme_pair_type find_extreme_pair (double* data, int rows, int cols, int rank, int size) {

    extreme_pair_type extreme_pair = {0,-1,-1};
    for (int i=0;i<rows-1;i++) {
        for (int j=i+1;j<rows;j++) {
            float dist_sq = vec_dist_sq(data+i*cols,data+j*cols,cols);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i[0] = i;
                extreme_pair.i[1] = j;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* get num_points from command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"num_points");
        return 1;
    }
    int num_points = atoi(argv[1]);

    /* There are 10000 MNIST images.  Each image is 28x28 = 784. */
    int rows = 10000; 
    int cols = 784;

    /* dynamically allocate memory for the data matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* read the binary mnist test file */
    vec_read_bin(data,rows*cols,"t10k-images-idx3-ubyte",16);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    /* find the extreme pair of the first num_points points */
    extreme_pair_type extreme_pair = find_extreme_pair(data,num_points,cols,rank,size);

    /* stop the timer */
    end_time = MPI_Wtime();

    /* print the results */
    printf ("rank %d elapsed time = %g\n",rank,end_time-start_time);
    printf ("rank %d Extreme Distance = %.2f\n",rank,sqrt(extreme_pair.dist_sq));
    printf ("rank %d Extreme Pair = %d %d\n",rank,extreme_pair.i[0],extreme_pair.i[1]);

    /* free the dynamically allocated memory */
    free (data);

    MPI_Finalize();
}

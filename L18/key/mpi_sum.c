#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef unsigned long long uint64;

int main(int argc, char **argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* get N from command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64 N = atol(argv[1]);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    /* calculate the sum */
    uint64 sum = 0;
    for (uint64 i = 1+rank; i <= N;i+=size) {
        sum += i;
    }

    /* all nonzero ranks send their partial sums to rank 0 */
    if (rank==0) {
	uint64 rank_sum;
	MPI_Status status;
	for (int source=1;source<size;source++) {
	    MPI_Recv(&rank_sum,1,MPI_UNSIGNED_LONG_LONG,source,0,MPI_COMM_WORLD,&status);
#ifdef DEBUG
	    printf ("rank 0 received message %llu from rank %d\n",rank_sum,source);
#endif
	    sum += rank_sum;
	}
    } else {
	int dest = 0;
	MPI_Send(&sum,1,MPI_UNSIGNED_LONG_LONG,dest,0,MPI_COMM_WORLD);
    }

    /* stop the timer */
    end_time = MPI_Wtime();

    if (rank==0) {
	printf ("sum = %llu, N*(N+1)/2 = %llu\n",sum,(N/2)*(N+1));
	printf ("elapsed time = %.4lf seconds\n",end_time-start_time);
    }

    MPI_Finalize();

}


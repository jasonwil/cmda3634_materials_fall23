#include <stdio.h>

int main () {

    int num_As = 0;
    int num_Bs = 0;
    int num_Cs = 0;
    int num_Ds = 0;
    int num_Fs = 0;

    double number;
    while (scanf("%lf",&number) == 1) {
        if (number < 60.0) {
            num_Fs += 1;
        } else if (number < 70.0) {
            num_Ds += 1;
        } else if (number < 80.0) {
            num_Cs += 1;
        } else if (number < 90.0) {
            num_Bs += 1;
        } else {
            num_As += 1;
        }
    }

    printf ("Number of A's = %d\n",num_As);
    printf ("Number of B's = %d\n",num_Bs);
    printf ("Number of C's = %d\n",num_Cs);
    printf ("Number of D's = %d\n",num_Ds);
    printf ("Number of F's = %d\n",num_Fs);

}
